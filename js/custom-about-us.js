  {
	"blocks": [{
			"type": "hero",
			"name": "Hero",
			"settings": [{
					"id": "bannerImage",
					"type": "image_picker",
					"label": "Image"
				},
				{
					"id": "alignment",
					"type": "select",
					"label": "Image alignment"
				},
                    "default": "center",
                          "options": [
                            {
                              "value": "top",
                              "label":"Top",
                              }
                            },
                            {
                              "value": "center",
                              "label":"Middle",
                              }
                            },
                            {
                              "value": "bottom",
                              "label": "Bottom",
                              }
                            }
                          ]

			]
		},

		{
			"type": "comparison",
			"name": "Compare",
			"settings": [{
					"id": "comparisonImage",
					"type": "image_picker",
					"label": "image"
				},
				{
					"id": "comparisonTitle",
					"type": "text",
					"label": "Title"
				},
				{
					"id": "comparisonSummary",
					"type": "textarea",
					"label": "Summary"
				},
				{
					"id": "comparisonButtonLink",
					"type": "url",
					"label": "Button Link"
				},
				{
					"id": "comparisonButtonText",
					"type": "text",
					"label": "Button Text"
				}
			]
		}
	]
}